import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  uploadForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required]
  });

  private uploadFile: FormData;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private emptyOptions = {
    headers: new HttpHeaders()
  };

  private httpFormData = {
    headers: new HttpHeaders({
      'Content-Type': 'multipart/form-data'
    })
  };

  private formdt = new FormData();
  constructor(private http: HttpClient, private fb: FormBuilder) {}

  ngOnInit(): void {}

  onFileSelect(event: any) {
    if (event.target.files.length > 0) {
      this.formdt.append('upload_file', event.target.files[0]);
    } else {
      console.log('No file');
    }
  }

  onSubmit() {
    console.log(this.formdt.get('upload_file'));
    this.formdt.append('setting', JSON.stringify(this.uploadForm.value));
    this.http
      .post<any>('http://localhost:8000/upload-file', this.formdt)
      .subscribe((res: any) => console.log(res));
  }

  TryGet() {
    console.log('Try Get api');
    this.http
      .get('http://localhost:8000/test')
      .subscribe(res => console.log(res));
  }

  TryPost() {
    console.log(`Try Post`);
    this.http
      .post<any>(
        'http://localhost:8000/trypost',
        { name: 'clover', message: 'Hello World' },
        this.httpOptions
      )
      .subscribe(res => console.log(res));
  }
}
